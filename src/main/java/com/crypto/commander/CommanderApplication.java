package com.crypto.commander;

import com.crypto.commander.service.Exchange;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;
import java.util.Set;

@EnableScheduling
@SpringBootApplication
public class CommanderApplication {

    public static final String binanceSpotURL = "http://185.181.10.88:9001/";
    public static final String binanceSpotWSURL = "ws://185.181.10.88:9001/";
    public static final String binanceFutureURL = "http://185.181.10.88:9002/";
    public static final String binanceFutureWSURL = "ws://185.181.10.88:9002/";
    public static final Set<String> pairs = Set.of("btcusdt");

    final Exchange binanceExchange;

    public CommanderApplication(Exchange binanceExchange) {
        this.binanceExchange = binanceExchange;
    }

    public static void main(String[] args) {
        SpringApplication.run(CommanderApplication.class, args);
    }

    @PostConstruct
    public void start() {

        binanceExchange.setExchange("binance");
        binanceExchange.setExchangeSpotUrl(binanceSpotURL);
        binanceExchange.setExchangeSpotWSUrl(binanceSpotWSURL);
        binanceExchange.setExchangeFutureUrl(binanceFutureURL);
        binanceExchange.setExchangeFutureWSUrl(binanceFutureWSURL);
        binanceExchange.setPairs(pairs);
        binanceExchange.start();
    }
}