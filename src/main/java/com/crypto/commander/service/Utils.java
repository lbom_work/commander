package com.crypto.commander.service;

import com.crypto.commander.config.AppConfig.MDType;
import com.crypto.commander.repo.messages.*;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class Utils {

    /**
     * @return LocalDateTime converted from epoch
     */
    public static LocalDateTime epochMilliToLocalDateTime(long epoch) {
        Instant instant = Instant.ofEpochMilli(epoch);
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    }

    /**
     * @return the current time in seconds since epoch
     */
    public static int currentTimeInSecs() {
        long currentTimeMS = System.currentTimeMillis();
        return (int) (currentTimeMS / 1000);
    }

    /**
     * Function which convert market data type to dto .class for jackson
     * @param  type - enum market of message (SPOT, FUTURE)
     * @return DTO message .class
     */
    public static JavaType getClassByType(MDType type) {
        return switch (type) {
            case BAR   ->  getJavaType(BarDTO.class);
            case QUOTE ->  getJavaType(QuoteDTO.class);
            case TRADE ->  getJavaType(TradeDTO.class);
            case OI    ->  getJavaType(OiDTO.class);
            case RATE  ->  getJavaType(RateDTO.class);
            case RATIO ->  getJavaType(RatioDTO.class);
        };
    }

    /**
     * Function which creates jackson JavaType from reflection DTO type
     * @param  type - reflection DTO type
     * @return DTO message .class
     */
    public static JavaType getJavaType(Type type) {
        return TypeFactory.defaultInstance().constructType(type);
    }
}