package com.crypto.commander.service;

import com.crypto.commander.controllers.ChannelsController;
import com.crypto.commander.controllers.DAOController;
import com.crypto.commander.controllers.QuotesController;
import com.crypto.commander.repo.messages.*;
import com.crypto.commander.repo.proccesable.LiveQuoteBook;
import com.crypto.commander.service.database.*;
import com.crypto.commander.service.security.JWTToken;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.logging.log4j.util.TriConsumer;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.Disposable;
import reactor.util.retry.Retry;

import java.net.URI;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static com.crypto.commander.config.AppConfig.*;

/**
 * Main exchange class - for now only market data consumer, in next updates - order routing, account data
 */
@Slf4j
@Component
@Scope("prototype")
public class Exchange extends Thread {

    @Getter @Setter private Set<String> pairs;
    @Getter @Setter private String exchange;
    @Getter @Setter private String exchangeSpotUrl;
    @Getter @Setter private String exchangeSpotWSUrl; // Websocket Spot Url
    @Getter @Setter private String exchangeFutureUrl;
    @Getter @Setter private String exchangeFutureWSUrl; // Websocket Futures Url

    @Getter private WebClient restClient;
    @Getter private WebSocketClient socketClient;
    @Getter private final JWTToken JWTToken;
    @Getter private final DAOController storage;
    @Getter private final ChannelsController channels;
    @Getter private final QuotesController quotes;

    private final BlockingQueue<String> terminated = new ArrayBlockingQueue<>(1);

    /**
     * Creates main exchange class - market, account data consumer, handler
     * @param storage  - database saving routing controller for DAO-s
     * @param JWTToken - generating access tokens for REST requests
     * @param quotes   - controller which holds mutable LiveQuoteBook and process calculations on it
     * @param channels - controller which holds all market data channels
     */
    public Exchange(DAOController storage, JWTToken JWTToken, QuotesController quotes, ChannelsController channels) {
        this.storage = storage;
        this.JWTToken = JWTToken;
        this.quotes = quotes;
        this.channels = channels;
    }

    /**
     * When a thread starts, it creates REST, WebSocket clients and inits subscriptions
     */
    public void run() {
        socketClient = new ReactorNettyWebSocketClient();
        restClient = WebClient.builder()
                .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(REQUEST_BUFFER_SIZE))
                .build();
        quotes.setExchange(this);
        subscribeAll();
    }

    /**
     * Subscribing to all market data
     */
    public void subscribeAll() {
        subscribeMarket(Market.SPOT, this::createBasicChannels);
        subscribeMarket(Market.FUTURE, this::createFuturesChannels);
    }

    /**
     * Subscribing to one market (Spot or Futures). Getting initial order book snapshot
     * @param market - enum market of message (SPOT, FUTURE)
     * @param subscriber - function to open market data channels for all market data types
     */
    public void subscribeMarket(Market market, TriConsumer<Market, QuoteDTO, String> subscriber) {

        String requestUrl = switch(market) {
            case SPOT ->  exchangeSpotUrl;
            case FUTURE -> exchangeFutureUrl;
        };

        pairs.forEach(pair -> restClient.get()
                .uri(requestUrl, uriBuilder -> uriBuilder
                        .path(restUriBuilder(requestUrl, MDType.QUOTE))
                        .queryParam("pair", pair)
                        .build())
                .headers(header -> header.setBearerAuth(JWTToken.newToken(exchange)))
                .retrieve()
                .bodyToMono(QuoteDTO.class)
                .doOnSuccess(success -> {
                    if (!terminated.isEmpty()) terminated.remove();
                })
                .doOnError(error -> onError(market, MDType.QUOTE,"getting initial Quote Snap", error))
                .retryWhen(Retry.fixedDelay(RETRY_COUNT, Duration.ofSeconds(RETRY_TIMEOUT)))
                .subscribe(quoteDTO -> {
                    quotes.put(market, pair, new LiveQuoteBook(quoteDTO, (QuotesDAO) storage.getDAO(market, MDType.QUOTE)));
                    subscriber.accept(market, quoteDTO, pair);
                })
        );
    }
    /**
     * Subscribing to basic market data channels - trades, quotes, bars.
     * @param market - enum market of message (SPOT, FUTURE)
     * @param dto - initial order book snapshot
     * @param pair - trade pair from Exchang
     */
    private void createBasicChannels(Market market, QuoteDTO dto, String pair) {
        createChannel(market, MDType.TRADE, pair, this::onSave);
        createChannel(market, MDType.BAR, pair, this::onSave);
        createChannel(market, MDType.QUOTE, pair, this::onQuote);
    }

    /**
     * Subscribing to future market data channels - OI, Rate, Ratio.
     * @param market - enum market of message (SPOT, FUTURE)
     * @param dto - initial order book snapshot
     * @param pair - trade pair from Exchange
     */
    public void createFuturesChannels(Market market, QuoteDTO dto, String pair) {
        createBasicChannels(market, dto, pair);
        createChannel(market, MDType.OI, pair, this::onSave);
        createChannel(market, MDType.RATE, pair, this::onSave);
        createChannel(market, MDType.RATIO, pair, this::onSave);
    }

    /**
     * UnSubscribing from all market data channels.
     */
    public void unsubscribeAll() {
        channels.closeAll();
    }

    /**
     * Subscribe to channel of type <T>
     * @param market - enum market of message (SPOT, FUTURE)
     * @param mdType - enum type of message (BAR, TRADE...)
     * @param pair - trade pair from Exchange
     * @param handler - a function which handles the incoming messages must use one of the SaveService impl
     */
    private void createChannel(Market market, MDType mdType, String pair, TriConsumer<Market, MDType, String> handler){

        HttpHeaders headers = new HttpHeaders();
        Disposable channel = socketClient.execute(URI.create(wsUrlBuilder(market, mdType, pair)),
                headers,
                session -> session.receive()
                        .doOnNext(message -> handler.accept(market, mdType, message.getPayloadAsText()))
                        .doOnError(error -> onError(market, mdType, "socket data receiving", error))
                        .doOnTerminate(this::onTerminate)
                        .retry(RETRY_COUNT)
                        .then()).subscribe();
        channels.addChannel(market, mdType, pair, channel);
    }

    /**
     * Database saving handler
     * @param market - enum market of message (SPOT, FUTURE)
     * @param mdType - enum type of message (BAR, TRADE...)
     * @param message - incoming message (TradeDTO, ...)
     */
    private <T> void onSave(Market market, MDType mdType, String message) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            T dto = objectMapper.readValue(message, Utils.getClassByType(mdType));
            storage.save(market, mdType, dto);
        } catch (Exception e) {
            log.error("Error while saving {}, {}", mdType.toString(), e.getMessage());
        }
    }

    /**
     * Quote processing and saving handler
     * @param market - enum market of message (SPOT, FUTURE)
     * @param type - enum type of message (BAR, TRADE...)
     * @param message - incoming message (TradeDTO, ...)
     */
    private void onQuote(Market market, MDType type, String message) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            QuoteDTO dto = objectMapper.readValue(message, QuoteDTO.class);
            quotes.get(market, dto.getPair()).ifPresent(liveQuoteBook -> liveQuoteBook.update(dto));
            storage.save(market, type, dto);
        } catch (NullPointerException|JsonProcessingException e) {
            log.error("Error while deserialize QuoteBookDTO {}", e.getMessage());
        }
    }

    /**
     * Database saving handler
     * @param market - enum market of process (SPOT, FUTURE)
     * @param process - process witch fails
     * @param e - exception
     */
    private void onError(Market market, MDType type, String process, Throwable e) {
        log.error("Error in {}, market: {}, type: {}, cause: {}", process, market.name(), type.name(), e.getMessage());
    }

    /**
     * Connection termination handler
     * Reduce all terminate errors to one by sending to BlockingQueue and initialize re-subscribing
     */
    private void onTerminate() {
        if (terminated.add("terminated")) {
            log.error(" <--------Connection terminated--------> ");
            subscribeAll();
        }
    }

    /**
     * Builds URL for REST requests, using message class switcher
     * @param type - enum type of message (BAR, TRADE...)
     * @return request URL
     */
    private String restUriBuilder(String reqURL, MDType type) {

        String uri = switch(type) {
            case QUOTE ->  QUOTES_SNAP_PATH;
            case TRADE -> TRADES_PATH;
            case BAR -> BARS_PATH;
            case RATE -> RATE_PATH;
            case RATIO -> RATIO_PATH;
            case OI -> OI_PATH;
        };

        log.info("Request: {}", reqURL + uri);
        return uri;
    }

    /**
     * Builds URL for WebSocket requests, using message class switcher
     * @param market - exchange market (SPOT, FUTURE)
     * @param type - exchange market data type
     * @param pair - exchange pair
     * @return request URL
     */
    private String wsUrlBuilder(Market market, MDType type, String pair) {

        String baseUrl = (market.equals(Market.SPOT)) ? exchangeSpotWSUrl: exchangeFutureWSUrl;
        String requestUrl = switch (type) {
            case BAR -> baseUrl + BARS_PATH + "/" + pair + "/" + BAR_INTERVAL;
            case QUOTE -> baseUrl + QUOTES_UPDATES_PATH + "/" + pair;
            case TRADE -> baseUrl + TRADES_PATH + "/" + pair;
            case OI -> baseUrl + OI_PATH + "/" + pair;
            case RATE -> baseUrl + RATE_PATH + "/" + pair;
            case RATIO -> baseUrl + RATIO_PATH + "/" + pair;
        };

        log.info("Request: {}", requestUrl);
        return requestUrl;
    }
}