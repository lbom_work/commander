package com.crypto.commander.service.database;

import com.crypto.commander.repo.messages.BarDTO;
import com.crypto.commander.service.Utils;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Slf4j
@Service
@Scope(value = "prototype")
public class BarsDAO implements DAO<BarDTO> {

    @Setter
    private JdbcTemplate jdbc;

    public BarsDAO() {}

    public void save(BarDTO barDTO) {

        String request = "INSERT INTO " + pgScheme + "bars_"
                    + barDTO.getExchange() + "_" + barDTO.getPair() + "_" + barDTO.getPeriod() +
                    "(exchange, market, pair, period, open_time, close_time, " +
                    "open, high, low, close, trades_count, " +
                    "base_volume, cost_volume, base_buy_volume, base_sell_volume, cost_buy_volume, cost_sell_volume) " +
                " VALUES (?, ?, ?, ?, ?,  ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?,  ?, ?)";
        try {
            jdbc.update(request,
                    barDTO.getExchange(), barDTO.getMarket(), barDTO.getPair(), barDTO.getPeriod(),
                    Utils.epochMilliToLocalDateTime(barDTO.getOpenTime()),
                    Utils.epochMilliToLocalDateTime(barDTO.getCloseTime()),
                    barDTO.getOpen(), barDTO.getHigh(), barDTO.getLow(), barDTO.getClose(), barDTO.getTradesCount(),
                    barDTO.getBaseVolume(), barDTO.getCostVolume(),
                    barDTO.getBaseBuyVolume(), barDTO.getBaseSellVolume(),
                    barDTO.getCostBuyVolume(), barDTO.getCostSellVolume());
        } catch (DataAccessException e) {
            log.error("Error in saving bar: {}", e.getMessage());
        }
    }

    public void update(BarDTO barDTO) {

    }

    public void delete(int id) {

    }

    public Optional<BarDTO> get(int id, String... args) {
        return Optional.of(new BarDTO());
    }

    public Collection<BarDTO> getByTimestampRange(LocalDateTime begin, LocalDateTime end) {
        return new ArrayList<>();
    }
}