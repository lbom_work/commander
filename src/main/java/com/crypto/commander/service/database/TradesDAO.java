package com.crypto.commander.service.database;

import com.crypto.commander.repo.messages.TradeDTO;
import com.crypto.commander.service.Utils;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Slf4j
@Service
@Scope(value = "prototype")
public class TradesDAO implements DAO<TradeDTO> {

    @Setter
    private JdbcTemplate jdbc;

    public TradesDAO() {}

    public void save(TradeDTO tradeDTO) {

        String request = "INSERT INTO " + pgScheme + "trades_" + tradeDTO.getExchange() + "_" + tradeDTO.getPair() +
                        "(exchange, market, pair, id, buy, price, volume, cost_volume, date) " +
                        " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
             jdbc.update(request,
                    tradeDTO.getExchange(), tradeDTO.getMarket(), tradeDTO.getPair(),
                    tradeDTO.getId(), tradeDTO.isBuy(), tradeDTO.getPrice(), tradeDTO.getVolume(), tradeDTO.getCostVolume(),
                    Utils.epochMilliToLocalDateTime(tradeDTO.getTimestamp()));
        } catch (DataAccessException e) {
            log.error("Error in saving trade: {}", e.getMessage());
        }
    }

    public void update(TradeDTO tradeDTO) {

    }
    public void delete(int id) {

    }

    public Optional<TradeDTO> get(int id, String... args) {
        return Optional.of(new TradeDTO());
    }

    public Collection<TradeDTO> getByTimestampRange(LocalDateTime begin, LocalDateTime end) {
        ArrayList<TradeDTO> bars = new ArrayList<>();
        return bars;
    }
}