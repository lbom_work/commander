package com.crypto.commander.service.database;

import com.crypto.commander.repo.messages.OiDTO;
import com.crypto.commander.service.Utils;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Slf4j
@Service
@Scope(value = "prototype")
public class OiDAO implements DAO<OiDTO> {

    @Setter
    private JdbcTemplate jdbc;

    public OiDAO() {}

    public void save(OiDTO dto) {

        String request = "INSERT INTO " + pgScheme  + "oi_" + dto.getExchange() + "_" + dto.getPair() +
                        " (exchange, market, pair, oi, oi_cost, date) " +
                        " VALUES (?, ?, ?, ?, ?, ?)";
        try {
             jdbc.update(request, dto.getExchange(), dto.getMarket(), dto.getPair(),
                                  dto.getSumOpenInterest(), dto.getSumOpenInterestCost(),
                                  Utils.epochMilliToLocalDateTime(dto.getTimestamp()));
        } catch (DataAccessException e) {
            log.error("Error in saving oi: {}", e.getMessage());
        }
    }

    public void update(OiDTO tradeDTO) {}

    public void delete(int id) {}

    public Optional<OiDTO> get(int id, String... args) {
        return Optional.of(new OiDTO());
    }

    public Collection<OiDTO> getByTimestampRange(LocalDateTime begin, LocalDateTime end) {
        ArrayList<OiDTO> bars = new ArrayList<>();
        return bars;
    }
}