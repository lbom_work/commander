package com.crypto.commander.service.database;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

public interface DAO<T> {

    String pgScheme = "crypto.";

    void save(T t);
    void update(T t);
    void delete(int id);

    Optional <T> get(int id, String... args);
    Collection <T> getByTimestampRange(LocalDateTime begin, LocalDateTime end);
}