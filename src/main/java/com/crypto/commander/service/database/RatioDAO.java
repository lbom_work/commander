package com.crypto.commander.service.database;

import com.crypto.commander.config.AppConfig.Ratio;
import com.crypto.commander.repo.messages.RatioDTO;
import com.crypto.commander.service.Utils;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Slf4j
@Service
@Scope(value = "prototype")
public class RatioDAO implements DAO<RatioDTO> {

    @Setter
    private JdbcTemplate jdbc;

    public RatioDAO() {}

    public void save(RatioDTO ratioDTO) {

        Ratio type_id = Ratio.values()[ratioDTO.getType_id()];

        String table = switch (type_id) {
            case GLOBAL -> "global_ratio_";
            case TAKER -> "taker_ratio_";
            case TOP_ACCOUNT -> "top_account_ratio_";
            case TOP_POSITION -> "top_position_ratio_";
        };

        String request = "INSERT INTO " + pgScheme + table + ratioDTO.getExchange() + "_" + ratioDTO.getPair() +
                        " (exchange, market, pair, buy_sum, sell_sum,   ratio, date) " +
                        " VALUES (?, ?, ?, ?, ?,   ?, ?)";
        try {
            jdbc.update(request,
                    ratioDTO.getExchange(), ratioDTO.getMarket(), ratioDTO.getPair(),
                    ratioDTO.getBuySum(), ratioDTO.getSellSum(), ratioDTO.getRatio(),
                    Utils.epochMilliToLocalDateTime(ratioDTO.getTimestamp()));
        } catch (DataAccessException e) {
            log.error("Error in saving ratio: {}", e.getMessage());
        }
    }

    public void update(RatioDTO tradeDTO) {

    }
    public void delete(int id) {

    }

    public Optional<RatioDTO> get(int id, String... args) {
        return Optional.of(new RatioDTO());
    }

    public Collection<RatioDTO> getByTimestampRange(LocalDateTime begin, LocalDateTime end) {
        ArrayList<RatioDTO> bars = new ArrayList<>();
        return bars;
    }
}