package com.crypto.commander.service.database;

import com.crypto.commander.repo.messages.RateDTO;
import com.crypto.commander.service.Utils;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Slf4j
@Service
@Scope(value = "prototype")
public class RateDAO implements DAO<RateDTO> {

    @Setter
    private JdbcTemplate jdbc;

    public RateDAO() {}

    public void save(RateDTO dto) {

        String request = "INSERT INTO " + pgScheme + "rate_" + dto.getExchange() + "_" + dto.getPair() +
                        " (exchange, market, pair, funding_rate , date) " +
                        " VALUES (?, ?, ?, ?, ?)";
        try {
             jdbc.update(request, dto.getExchange(), dto.getMarket(), dto.getPair(),
                                  dto.getFundingRate(), Utils.epochMilliToLocalDateTime(dto.getTimestamp()));
        } catch (DataAccessException e) {
            log.error("Error in saving rate: {}", e.getMessage());
        }
    }

    public void update(RateDTO tradeDTO) {

    }
    public void delete(int id) {

    }

    public Optional<RateDTO> get(int id, String... args) {
        return Optional.of(new RateDTO());
    }

    public Collection<RateDTO> getByTimestampRange(LocalDateTime begin, LocalDateTime end) {
        ArrayList<RateDTO> values = new ArrayList<>();
        return values;
    }
}