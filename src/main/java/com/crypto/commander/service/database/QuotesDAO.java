package com.crypto.commander.service.database;

import com.crypto.commander.repo.messages.QuoteDTO;
import com.crypto.commander.repo.proccesable.LiveQuoteBook;
import com.crypto.commander.service.Utils;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Service
@Scope(value = "prototype")
public class QuotesDAO implements DAO<QuoteDTO> {

    @Setter
    private JdbcTemplate jdbc;
    @Setter
    private TransactionTemplate transactionTemplate;

    public QuotesDAO() {}

    /**
     * Saving order book snapshot via transaction,
     * if inserting of asks or bids part failed then roll backing transaction
     * @param book - a function which handles the incoming messages must use one of the SaveService impl
     */
    public void save(QuoteDTO book) {

        transactionTemplate.execute(transactionStatus -> {

            try {
                String request = "INSERT INTO " + pgScheme + book.getExchange() + "_" + book.getPair() + "_upds" +
                        " (exchange, market, pair, date) VALUES (?, ?, ?, ?)";
                KeyHolder holder = new GeneratedKeyHolder();
                jdbc.update(prep -> {
                    PreparedStatement ps = prep.prepareStatement(request, new String[]{"id"});
                    ps.setObject(1, book.getExchange());
                    ps.setObject(2, book.getMarket());
                    ps.setObject(3, book.getPair());
                    ps.setObject(4, Utils.epochMilliToLocalDateTime(book.getTimestamp()));
                    return ps;
                }, holder);
                long id = Objects.requireNonNull(holder.getKey()).longValue();
                jdbc.execute(createRequest(id, book.getPair() + "_upds_asks", book.getAsks()));
                jdbc.execute(createRequest(id, book.getPair() + "_upds_bids", book.getBids()));
            } catch (Exception ex) {
                transactionStatus.setRollbackOnly();
                log.error("Error in quotes updates transaction: {}", ex.getMessage());
                /* todo send failed request to reliability manager */
            }

            return null;
        });
    }

    public void saveSnap(LiveQuoteBook book) {

        transactionTemplate.execute(transactionStatus -> {

            String request = "INSERT INTO " + pgScheme
                + book.getExchange() + "_" + book.getPair() + "_snaps" +
                " (exchange, market, pair, date,  " +
                "asks_volume, bids_volume, near_asks_vol, near_bids_vol) " +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            try {
                KeyHolder holder = new GeneratedKeyHolder();
                jdbc.update(prep -> {
                    PreparedStatement ps = prep.prepareStatement(request, new String[]{"id"});
                    ps.setObject(1, book.getExchange());
                    ps.setObject(2, book.getMarket());
                    ps.setObject(3, book.getPair());
                    ps.setObject(4, Utils.epochMilliToLocalDateTime(book.getTimestamp()));
                    ps.setObject(5, book.getAsksVolume());
                    ps.setObject(6, book.getBidsVolume());
                    ps.setObject(7, book.getNearestAsksVolume());
                    ps.setObject(8, book.getNearestBidsVolume());
                    return ps;
                }, holder);
                long id = Objects.requireNonNull(holder.getKey()).longValue();
                jdbc.execute(createRequest(id, book.getPair() +"_snaps_asks", book.getAsks()));
                jdbc.execute(createRequest(id, book.getPair() +"_snaps_bids", book.getBids()));
            } catch (Exception ex) {
                transactionStatus.setRollbackOnly();
                log.error("Error in quotes snaps transaction: {}", ex.getMessage());
                /* todo send failed request to reliability manager */
            }

            return null;
        });
    }

    public void update(QuoteDTO book) {
    }
    public void delete(int id) {
    }

    public Optional<QuoteDTO> get(int id, String... args) {
        return Optional.of(new QuoteDTO());
    }

    public Collection<QuoteDTO> getByTimestampRange(LocalDateTime begin, LocalDateTime end) {
        ArrayList<QuoteDTO> quotes = new ArrayList<>();
        return quotes;
    }

    private String createRequest(long id, String suffix, Map<BigDecimal, BigDecimal> values) {
        StringBuilder request = new StringBuilder("INSERT INTO crypto.binance_" + suffix + " (root_id, price, volume) VALUES ");
        values.forEach((key, value) -> request
                .append("(").append(id)
                .append(",").append(key)
                .append(",").append(value).append("),"));
        request.deleteCharAt(request.length() - 1);
        return request.toString();
    }
}