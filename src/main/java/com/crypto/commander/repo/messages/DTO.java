package com.crypto.commander.repo.messages;

public interface DTO {
    String getExchange();
    String getMarket();
    String getPair();
    long getTimestamp();
}