package com.crypto.commander.repo.messages;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.TreeMap;

/**
 *  DTO class fo consuming quotes messages (snapshots via rest and updates via websocket)
 */
@ToString
@NoArgsConstructor
public final class QuoteDTO implements DTO {

    @Getter private String exchange;                  //  exchange: b - binance
    @Getter private String market;                    //  market type: s - spot, f - futures
    @Getter private String pair;                      //  pair: btcusdt
    @Getter private long   timestamp;

    @Getter private TreeMap<BigDecimal, BigDecimal> asks;
    @Getter private TreeMap<BigDecimal, BigDecimal> bids;

    public QuoteDTO(String exchange, String market, String pair,
                    TreeMap<BigDecimal, BigDecimal> asks,
                    TreeMap<BigDecimal, BigDecimal> bids,
                    long timestamp) {
        this.exchange = exchange;
        this.market = market;
        this.pair = pair;
        this.asks = asks;
        this.bids = bids;
        this.timestamp = timestamp;
    }
}