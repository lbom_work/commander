package com.crypto.commander.repo.messages;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 *  DTO class fo consuming ratios messages
 *  (Top account ratio, Top position ratio, global ratio, taker ratio)
 */
@ToString
@NoArgsConstructor
public final class RatioDTO implements DTO {

    @Getter @Setter private String pair;
    @Getter @Setter private int type_id;
    @Getter private String exchange;
    @Getter private String market;
    @Getter private BigDecimal buySum;
    @Getter private BigDecimal ratio;
    @Getter private BigDecimal sellSum;
    @Getter private long timestamp;

    public RatioDTO(String pair, int type_id, String exchange, String market, BigDecimal buySum,
                    BigDecimal ratio, BigDecimal sellSum, long timestamp) {
        this.pair = pair;
        this.type_id = type_id;
        this.exchange = exchange;
        this.market = market;
        this.buySum = buySum;
        this.ratio = ratio;
        this.sellSum = sellSum;
        this.timestamp = timestamp;
    }
}