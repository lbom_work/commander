package com.crypto.commander.repo.messages;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

/**
 *  DTO class fo consuming rates messages
 */
@ToString
@NoArgsConstructor
public final class RateDTO implements DTO {

    @Getter private String exchange;                  //  exchange: b - binance
    @Getter private String market;                    //  market type: s - spot, f - futures
    @Getter private String pair;                      //  pair: btcusdt
    @Getter private long   fundingTime;
    @Getter private BigDecimal fundingRate;

    public RateDTO(String exchange, String market, String pair, BigDecimal fundingRate, long fundingTime) {
        this.exchange = exchange;
        this.market = market;
        this.pair = pair;
        this.fundingRate = fundingRate;
        this.fundingTime = fundingTime;
    }

    @Override
    public long getTimestamp(){
        return fundingTime;
    }
}