package com.crypto.commander.repo.messages;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

/**
 *  DTO class for consuming open interest messages
 */
@ToString
@NoArgsConstructor
public final class OiDTO implements DTO {

    @Getter private String exchange;                  //  exchange: b - binance
    @Getter private String market;                    //  market type: s - spot, f - futures
    @Getter private String pair;                      //  pair: btcusdt
    @Getter private long   timestamp;
    @Getter private BigDecimal sumOpenInterest;
    @Getter private BigDecimal sumOpenInterestCost;

    public OiDTO(String exchange, String market, String pair,
                 BigDecimal sumOpenInterest, BigDecimal sumOpenInterestCost, long timestamp) {
        this.exchange = exchange;
        this.market = market;
        this.pair = pair;
        this.sumOpenInterest = sumOpenInterest;
        this.sumOpenInterestCost = sumOpenInterestCost;
        this.timestamp = timestamp;
    }
}