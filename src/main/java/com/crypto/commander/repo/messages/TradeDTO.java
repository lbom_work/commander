package com.crypto.commander.repo.messages;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

/**
 *  DTO class fo consuming trades messages
 */
@ToString
@NoArgsConstructor
public final class TradeDTO implements DTO {

    @Getter private String exchange;                  //  exchange: b - binance
    @Getter private String market;                    //  market type: s - spot, f - futures
    @Getter private String pair;                      //  pair: btcusdt
    @Getter private long   timestamp;
    @Getter private long   id;
    @Getter private boolean buy;
    @Getter private BigDecimal price;
    @Getter private BigDecimal volume;
    @Getter private BigDecimal costVolume;

    public TradeDTO(String exchange, String market, String pair, long id,
                    boolean buy, BigDecimal price, BigDecimal volume, BigDecimal costVolume, long timestamp) {
        this.exchange = exchange;
        this.market = market;
        this.pair = pair;
        this.id = id;
        this.buy = buy;
        this.price = price;
        this.volume = volume;
        this.costVolume = costVolume;
        this.timestamp = timestamp;
    }
}
