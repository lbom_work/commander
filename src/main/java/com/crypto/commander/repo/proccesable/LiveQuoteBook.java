package com.crypto.commander.repo.proccesable;

import com.crypto.commander.repo.messages.QuoteDTO;
import com.crypto.commander.service.database.QuotesDAO;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.Consumer;

import static com.crypto.commander.config.AppConfig.*;

/**
 *  Live mutable quote book, encapsulates some calculation (volume sums)
 */
@Slf4j
@Component
public class LiveQuoteBook {

    public QuotesDAO dao;

    @Getter
    private String exchange;
    @Getter
    private String market;
    private Consumer<Long> consumer;
    @Getter
    private String pair;
    @Getter
    private long timestamp;
    @Getter
    private BigDecimal bidsVolume;
    @Getter
    private BigDecimal asksVolume;
    @Getter
    private BigDecimal nearestAsksVolume;
    @Getter
    private BigDecimal nearestBidsVolume;
    @Getter
    private ConcurrentSkipListMap<BigDecimal, BigDecimal> asks;
    @Getter
    private ConcurrentSkipListMap<BigDecimal, BigDecimal> bids;

    public LiveQuoteBook(){}

    /**
     * Creates new live order book from data of REST request with maximum depth
     * @param book - initial order book from REST service
     * @param dao - database saving service
     */
    public LiveQuoteBook(QuoteDTO book, QuotesDAO dao) {

        asks = new ConcurrentSkipListMap<>(book.getAsks());
        bids = new ConcurrentSkipListMap<>(Collections.reverseOrder());
        bids.putAll(book.getBids());
        exchange = book.getExchange();
        market = book.getMarket();
        if(market.equals("spot")) {
            consumer = this::spotConsumer;
        } else {
            consumer = this::futureConsumer;
        }
        pair = book.getPair();
        timestamp = book.getTimestamp();
        this.dao =dao;
        calculateVolumeSum();
        calculateNearestVolumeSum();
    }

    /**
     * Update order book by incoming message, recalculate volume sums with every incoming message,
     * save full snapshot to database every QUOTES_SNAP_PERIOD seconds
     * @param message - one of the Exchange pair
     */
    public void update(QuoteDTO message) {

        timestamp = message.getTimestamp();
        message.getAsks().forEach((k,v) -> {
            if (v.compareTo(BigDecimal.ZERO) == 0) {
                asks.remove(k);
            } else {
                asks.put(k, v);
            }
        });

        message.getBids().forEach((k,v) -> {
            if (v.compareTo(BigDecimal.ZERO) == 0) {
                bids.remove(k);
            } else {
                bids.put(k, v);
            }
        });
        calculateVolumeSum();
        calculateNearestVolumeSum();

        consumer.accept(timestamp);
    }

    /**
     *  Saving snapshots with spot market conditions
     */
    private void spotConsumer(long timestamp) {

        long seconds = timestamp / 1000;

        if (seconds % QUOTES_SNAP_PERIOD == 0)
            dao.saveSnap(this);
    }

    /**
     *  Saving snapshots with future market conditions
     */
    private void futureConsumer(long timestamp) {

        long seconds = timestamp / 1000;
        long milli = timestamp % 1000;

        if (seconds % QUOTES_SNAP_PERIOD == 0 && milli < QUOTES_SNAP_MILLI_FILTER)
            dao.saveSnap(this);
    }

    /**
     * Calculates total volume for asks and bids
     */
    public void calculateVolumeSum() {

        bidsVolume = bids.values().parallelStream().reduce(BigDecimal.ZERO, BigDecimal::add);
        asksVolume = asks.values().parallelStream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * Calculates volume of nearest to spread QUOTES_NEAREST_SIZE values for asks and bids
     */
    public void calculateNearestVolumeSum() {

        nearestAsksVolume = asks.values().stream()
                .limit(QUOTES_NEAREST_SIZE)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        nearestBidsVolume = bids.values().stream()
                .limit(QUOTES_NEAREST_SIZE)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}