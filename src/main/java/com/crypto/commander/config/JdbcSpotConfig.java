package com.crypto.commander.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.Objects;

/**
 * Configuration for spot databases
 */
@Configuration
public class JdbcSpotConfig {

    private final Environment env;

    public JdbcSpotConfig(Environment env) {
        this.env = env;
    }

    @Bean(name = "jdbcSpotQuotes")
    @Autowired
    public JdbcTemplate jdbcTemplate1(@Qualifier("sourceSpotQuotes") DataSource dsSlave1) {
        return new JdbcTemplate(dsSlave1);
    }
    @Bean(name = "jdbcSpotBars")
    @Autowired
    public JdbcTemplate jdbcTemplate2(@Qualifier("sourceSpotBars") DataSource dsSlave2) {
        return new JdbcTemplate(dsSlave2);
    }

    @Bean(name = "jdbcSpotTrades")
    @Autowired
    public JdbcTemplate jdbcTemplate3(@Qualifier("sourceSpotTrades") DataSource dsSlave3) {
        return new JdbcTemplate(dsSlave3);
    }



    @Bean(name = "sourceSpotQuotes")
    public DataSource spotQuotesDataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(Objects.requireNonNull(env.getProperty("spotquotes.datasource.driver-class-name")));
        dataSource.setUrl(env.getProperty("spotquotes.datasource.url"));
        dataSource.setUsername(env.getProperty("spotquotes.datasource.username"));
        dataSource.setPassword(env.getProperty("spotquotes.datasource.password"));

        return dataSource;
    }

    @Bean(name = "sourceSpotBars")
    public DataSource spotBarsDataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(Objects.requireNonNull(env.getProperty("spotbars.datasource.driver-class-name")));
        dataSource.setUrl(env.getProperty("spotbars.datasource.url"));
        dataSource.setUsername(env.getProperty("spotbars.datasource.username"));
        dataSource.setPassword(env.getProperty("spotbars.datasource.password"));

        return dataSource;
    }

    @Bean(name = "sourceSpotTrades")
    public DataSource spotTradesDataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(Objects.requireNonNull(env.getProperty("spottrades.datasource.driver-class-name")));
        dataSource.setUrl(env.getProperty("spottrades.datasource.url"));
        dataSource.setUsername(env.getProperty("spottrades.datasource.username"));
        dataSource.setPassword(env.getProperty("spottrades.datasource.password"));

        return dataSource;
    }
}