package com.crypto.commander.config;

import java.util.Map;
import java.util.Set;

/**
 * Parsing data configuration  - data types, market types, requests paths, saving & connection settings
 */
public class AppConfig {

    public  final static Map<String, String> exchangesURL = Map.of("binance", "binance.agent");
    public final static Set<String> ROOT_ROLES = Set.of("Market Data", "Orders", "Account", "Administration");

    /* Security configuration */

    public final static int TOKEN_LIVE_TIME = 10; // in secs
    public final static String PRIVATE_KEY_FILENAME = "/privateKey.pem";

    /* Market data configuration */

    public static final int  QUOTES_LIVE_CHECK_PERIOD = 10_000; // in ms
    public static final double  QUOTES_SNAP_PERIOD = 10; // in sec
    public static final double  QUOTES_SNAP_MILLI_FILTER = 500; // in sec
    public static final int  QUOTES_RECONNECTION_TIME = 5 * 60_000; // in ms
    public static final int  REQUEST_BUFFER_SIZE = 1_048_576 * 10; // in bytes
    public static final long QUOTES_NEAREST_SIZE = 30; // in number of dom levels
    public static final long RETRY_COUNT = 300; // number of re-connection attempts
    public static final long RETRY_TIMEOUT = 10; // timeout between attempts

    public static final String BARS_PATH = "bars";
    public static final String QUOTES_SNAP_PATH = "rest/quotes/snap";
    public static final String QUOTES_UPDATES_PATH = "quotes";
    public static final String TRADES_PATH = "trades";
    public static final String OI_PATH = "oi";
    public static final String RATIO_PATH = "ratio";
    public static final String RATE_PATH = "rate";

    public static final String BAR_INTERVAL = "1m";

    public enum Market {
        SPOT,
        FUTURE
    }

    public enum MDType {
        BAR,
        TRADE,
        QUOTE,
        OI,
        RATIO,
        RATE
    }

    public enum Ratio {
        GLOBAL,
        TAKER,
        TOP_ACCOUNT,
        TOP_POSITION
    }
}