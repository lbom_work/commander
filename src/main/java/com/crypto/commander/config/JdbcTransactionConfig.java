package com.crypto.commander.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;

/**
 * Configuration for transaction managers
 */
@Configuration
public class JdbcTransactionConfig {

    @Bean(name = "jdbcTxFutureQuotes")
    public TransactionTemplate txTemplateSpotQuotes(@Qualifier("jdbcFutureQuotesTransaction")
                                                            PlatformTransactionManager dataSource) {
        return new TransactionTemplate(dataSource);
    }

    @Bean(name = "jdbcTxSpotQuotes")
    public TransactionTemplate txTemplateFutureQuotes(@Qualifier("jdbcSpotQuotesTransaction")
                                                              PlatformTransactionManager manager) {
        return new TransactionTemplate(manager);
    }



    @Bean(name = "jdbcSpotQuotesTransaction")
    public PlatformTransactionManager txManagerSpotQuotes(@Qualifier("sourceSpotQuotes") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "jdbcFutureQuotesTransaction")
    public PlatformTransactionManager txManagerFuturesQuotes(@Qualifier("sourceFutureQuotes") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}