package com.crypto.commander.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import javax.sql.DataSource;
import java.util.Objects;

/**
 * Configuration for futures databases
 */
@Configuration
public class JdbcFutureConfig {

    private final Environment env;

    public JdbcFutureConfig(Environment env) {
        this.env = env;
    }

    @Bean(name = "jdbcFutureQuotes")
    @Autowired
    public JdbcTemplate jdbcTemplate1(@Qualifier("sourceFutureQuotes") DataSource dsSlave) {
        return new JdbcTemplate(dsSlave);
    }
    @Bean(name = "jdbcFutureBars")
    @Autowired
    public JdbcTemplate jdbcTemplate2(@Qualifier("sourceFutureBars") DataSource dsSlave) {
        return new JdbcTemplate(dsSlave);
    }

    @Bean(name = "jdbcFutureTrades")
    @Autowired
    public JdbcTemplate jdbcTemplate3(@Qualifier("sourceFutureTrades") DataSource dsSlave) {
        return new JdbcTemplate(dsSlave);
    }

    @Bean(name = "jdbcFutureIndicators")
    @Autowired
    public JdbcTemplate jdbcTemplate4(@Qualifier("sourceFutureIndicators") DataSource dsSlave) {
        return new JdbcTemplate(dsSlave);
    }


    @Bean(name = "sourceFutureBars")
    public DataSource spotBarsDataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(Objects.requireNonNull(env.getProperty("futurebars.datasource.driver-class-name")));
        dataSource.setUrl(env.getProperty("futurebars.datasource.url"));
        dataSource.setUsername(env.getProperty("futurebars.datasource.username"));
        dataSource.setPassword(env.getProperty("futurebars.datasource.password"));

        return dataSource;
    }

    @Bean(name = "sourceFutureTrades")
    public DataSource spotTradesDataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(Objects.requireNonNull(env.getProperty("futuretrades.datasource.driver-class-name")));
        dataSource.setUrl(env.getProperty("futuretrades.datasource.url"));
        dataSource.setUsername(env.getProperty("futuretrades.datasource.username"));
        dataSource.setPassword(env.getProperty("futuretrades.datasource.password"));

        return dataSource;
    }

    @Primary
    @Bean(name = "sourceFutureQuotes")
    public DataSource spotQuotesDataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(Objects.requireNonNull(env.getProperty("futurequotes.datasource.driver-class-name")));
        dataSource.setUrl(env.getProperty("futurequotes.datasource.url"));
        dataSource.setUsername(env.getProperty("futurequotes.datasource.username"));
        dataSource.setPassword(env.getProperty("futurequotes.datasource.password"));

        return dataSource;
    }

    @Bean(name = "sourceFutureIndicators")
    public DataSource spotIndicatorsDataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(Objects.requireNonNull(env.getProperty("futureindicators.datasource.driver-class-name")));
        dataSource.setUrl(env.getProperty("futureindicators.datasource.url"));
        dataSource.setUsername(env.getProperty("futureindicators.datasource.username"));
        dataSource.setPassword(env.getProperty("Futureindicators.datasource.password"));

        return dataSource;
    }
}
