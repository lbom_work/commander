package com.crypto.commander.controllers;

import com.crypto.commander.config.AppConfig.Market;
import com.crypto.commander.config.AppConfig.MDType;
import com.crypto.commander.service.database.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;
import java.util.concurrent.ConcurrentHashMap;

/**
 *  Controller and router of DAO classes.
 *  Here I'm used some suppress warning annotations for raw types and unchecked notifications,
 *  because class is final and have access only via initial set DAO methods.
 *  (Future and spot markets)
 */
@Slf4j
@Service
public final class DAOController {

    @SuppressWarnings("rawtypes")
    private final ConcurrentHashMap<MDType, DAO> future_services;

    @SuppressWarnings("rawtypes")
    private final ConcurrentHashMap<MDType, DAO> spot_services;

    public DAOController() {
        future_services = new ConcurrentHashMap<>();
        spot_services = new ConcurrentHashMap<>();
    }

    /**
     * Returning dao for given market and data type
     * @param market - enum market of message (SPOT, FUTURE)
     * @param mdType - enum type of message (BAR, TRADE...)
     * @return DAO
     */
    @SuppressWarnings("rawtypes")
    public DAO getDAO(Market market, MDType mdType) {

        if (market.equals(Market.SPOT)) {
            return switch (mdType) {
                case BAR   -> spot_services.get(MDType.BAR);
                case QUOTE -> spot_services.get(MDType.QUOTE);
                case TRADE -> spot_services.get(MDType.TRADE);
                default    -> {
                    log.error("Data mdType: " + mdType + "is not supported by spot market");
                    throw new IllegalStateException("Unexpected value: " + mdType);
                }
            };
        } else {
            return switch (mdType) {
                case BAR   -> future_services.get(MDType.BAR);
                case QUOTE -> future_services.get(MDType.QUOTE);
                case TRADE -> future_services.get(MDType.TRADE);
                case OI    -> future_services.get(MDType.OI);
                case RATE  -> future_services.get(MDType.RATE);
                case RATIO -> future_services.get(MDType.RATIO);
            };
        }
    }

    /**
     * Saving exchange market data message with dao depends on given market and data type
     * @param market - enum market of message (SPOT, FUTURE)
     * @param mdType - enum mdType of message (BAR, TRADE...)
     * @param dto    - message from exchange
     */
    @SuppressWarnings("unchecked")
    public <T> void save(Market market, MDType mdType, T dto) {
        if (market.equals(Market.SPOT)) {
            switch (mdType) {
                case BAR   -> spot_services.get(MDType.BAR).save(dto);
                case QUOTE -> spot_services.get(MDType.QUOTE).save(dto);
                case TRADE -> spot_services.get(MDType.TRADE).save(dto);
                default    -> log.error("Data mdType: " + mdType + "is not supported by spot market");
            }
        } else {
            switch (mdType) {
                case BAR   -> future_services.get(MDType.BAR).save(dto);
                case QUOTE -> future_services.get(MDType.QUOTE).save(dto);
                case TRADE -> future_services.get(MDType.TRADE).save(dto);
                case OI    -> future_services.get(MDType.OI).save(dto);
                case RATE  -> future_services.get(MDType.RATE).save(dto);
                case RATIO -> future_services.get(MDType.RATIO).save(dto);
            }
        }
    }

    /**
     * Here is initial dao configuration part
     */
    @Autowired
    public void setSpotQuotesDAO(@Qualifier("jdbcSpotQuotes") JdbcTemplate jdbc,
                                 @Qualifier("jdbcTxSpotQuotes") TransactionTemplate tx, QuotesDAO dao) {
        dao.setJdbc(jdbc);
        dao.setTransactionTemplate(tx);
        spot_services.put(MDType.QUOTE, dao);
    }

    @Autowired
    public void setSpotTradesDAO(@Qualifier("jdbcSpotTrades") JdbcTemplate jdbc, TradesDAO dao) {
        dao.setJdbc(jdbc);
        spot_services.put(MDType.TRADE, dao);
    }

    @Autowired
    public void setSpotBarsDAO(@Qualifier("jdbcSpotBars") JdbcTemplate jdbc, BarsDAO dao) {
        dao.setJdbc(jdbc);
        spot_services.put(MDType.BAR, dao);
    }


    @Autowired
    public void setFutureQuotesDAO(@Qualifier("jdbcFutureQuotes") JdbcTemplate jdbc,
                                   @Qualifier("jdbcTxFutureQuotes") TransactionTemplate tx, QuotesDAO dao) {
        dao.setJdbc(jdbc);
        dao.setTransactionTemplate(tx);
        future_services.put(MDType.QUOTE, dao);
    }

    @Autowired
    public void setFutureTradesDAO(@Qualifier("jdbcFutureTrades") JdbcTemplate jdbc, TradesDAO dao) {
        dao.setJdbc(jdbc);
        future_services.put(MDType.TRADE, dao);
    }

    @Autowired
    public void setFutureBarsDAO(@Qualifier("jdbcFutureBars") JdbcTemplate jdbc, BarsDAO dao) {
        dao.setJdbc(jdbc);
        future_services.put(MDType.BAR, dao);
    }

    @Autowired
    public void setFutureIndicatorsDAO(@Qualifier("jdbcFutureIndicators") JdbcTemplate jdbc,
                                       RatioDAO ratioDao, OiDAO oiDao, RateDAO rateDao) {
        ratioDao.setJdbc(jdbc);
        oiDao.setJdbc(jdbc);
        rateDao.setJdbc(jdbc);
        future_services.put(MDType.OI, oiDao);
        future_services.put(MDType.RATE, rateDao);
        future_services.put(MDType.RATIO, ratioDao);
    }
}