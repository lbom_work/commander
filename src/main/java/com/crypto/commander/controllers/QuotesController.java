package com.crypto.commander.controllers;

import com.crypto.commander.config.AppConfig.Market;
import com.crypto.commander.repo.proccesable.LiveQuoteBook;
import com.crypto.commander.service.Exchange;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static com.crypto.commander.config.AppConfig.QUOTES_LIVE_CHECK_PERIOD;
import static com.crypto.commander.config.AppConfig.QUOTES_RECONNECTION_TIME;

/**
 * Storing all live mutable quote book and process different operations on it
 */
@Slf4j
@Controller
public class QuotesController {

    @Getter @Setter
    private Exchange exchange;
    @Getter
    private final ConcurrentHashMap<String, LiveQuoteBook> future_map = new ConcurrentHashMap<>();
    @Getter
    private final ConcurrentHashMap<String, LiveQuoteBook> spot_map = new ConcurrentHashMap<>();

    public QuotesController() {}

    /**
     * Checking quote book actuality and if it's dead sending notification to reliability processor
     */
    @Scheduled(fixedRate = QUOTES_LIVE_CHECK_PERIOD)
    private void checkAliveness() {

        calculateAliveness(future_map);
        calculateAliveness(spot_map);

        /* todo send dead event to reliability processor */
    }

    private void calculateAliveness(ConcurrentHashMap<String, LiveQuoteBook> map) {

        map.forEach( (v,k) -> {
            long diff = System.currentTimeMillis() - k.getTimestamp();
            if (diff > QUOTES_LIVE_CHECK_PERIOD) {
                log.info("QuoteBook {} freshness lag: {} ms", k.getMarket() , diff);
                if (diff > QUOTES_RECONNECTION_TIME) {
                    exchange.unsubscribeAll();
                    exchange.subscribeAll();
                }
            }
        });
    }

    public void put(Market market, String pair, LiveQuoteBook book) {
        switch(market) {
            case SPOT -> spot_map.put(pair, book);
            case FUTURE -> future_map.put(pair, book);
        }
    }

    public Optional<LiveQuoteBook> get(Market market, String pair) {
        return switch(market) {
            case SPOT -> Optional.ofNullable(spot_map.get(pair));
            case FUTURE -> Optional.ofNullable(future_map.get(pair));
        };
    }
}