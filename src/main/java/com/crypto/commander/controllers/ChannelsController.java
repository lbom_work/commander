package com.crypto.commander.controllers;

import com.crypto.commander.config.AppConfig.MDType;
import com.crypto.commander.config.AppConfig.Market;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import reactor.core.Disposable;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import lombok.Getter;

/**
 *  Controller class to manipulate WebSocket channels
 *  (Future and spot markets)
 */
@Slf4j
@Controller
public class ChannelsController {

    @Getter final ConcurrentHashMap<String, Disposable> futureChannels;
    @Getter final ConcurrentHashMap<String, Disposable> spotChannels;

    public ChannelsController(){
        this.futureChannels = new ConcurrentHashMap<>();
        this.spotChannels = new ConcurrentHashMap<>();
    }

    public void addChannel(Market type, MDType mdType, String pair, Disposable channel){
        switch(type) {
            case SPOT   -> spotChannels.put(mdType.name() + pair, channel);
            case FUTURE -> futureChannels.put(mdType.name() + pair, channel);
        }
    }

    public void closeChannel(Market type, MDType mdType, String pair){
        String key = mdType.name() + pair;
        switch(type) {
            case SPOT   -> close(spotChannels, key);
            case FUTURE -> close(futureChannels, key);
        }
    }

    public void close(ConcurrentHashMap<String, Disposable> channels, String key) {
        Optional.ofNullable(channels.get(key))
                .ifPresent(Disposable::dispose);
        channels.remove(key);
    }

    public void closeAll(){
        spotChannels.values().forEach(Disposable::dispose);
        spotChannels.clear();
        futureChannels.values().forEach(Disposable::dispose);
        futureChannels.clear();
    }
}